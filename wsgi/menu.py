#coding: utf-8

# 第一層標題
m1 = "<a href='/about'>有關本站</a>"
m11 = "<a href='/'>Brython程式區</a>"
m111 = "<a href=''>四連桿機構</a>"
m112 = "<a href=''>零件體積表列</a>"
# 第一層標題
m2 = "<a href=''>期中報告</a>"
m21 = "<a href='/mid1'>solidedge04</a>"
m22 = "<a href='/mid2'>solidedge07</a>"
m23 = "<a href='/mid3'>solidedge18</a>"
# 第一層標題
m4 = "<a href=''>組員介紹</a>"
m41 = "<a href='/introMember1'>40123220</a>"
m42 = "<a href='/introMember2'>40123253</a>"
m43 = "<a href='/introMember3'>40123231</a>"
m44 = "<a href='/introMember4'>40123222</a>"
m45 = "<a href='/introMember5'>40123209</a>"
m46 = "<a href='/introMember6'>40123232</a>"
# 第一層標題
m5 = "<a href=''>期末報告</a>"
m51 = "<a href='/test1'>solvespace零組件</a>"
m52 = "<a href='/test2'>solvespace組合件</a>"
m53 = "<a href='/test3'>creo零組件</a>"
m54 = "<a href='/test4'>VREP模擬</a>"

# 請注意, 數列第一元素為主表單, 隨後則為其子表單
表單數列 = [ \
        [m1, [m11, m111, m112], ],  \
        [m2, m21, m22, m23, ],  \
        [m4, m41, m42, m43, m44,m45,m46], \
        [m5, m51, m52, m53, m54,]
        ]

def SequenceToUnorderedList(seq, level=0):
    # 只讓最外圍 ul 標註 class = 'nav'
    if level == 0:
        html_code = "<ul class='nav'>"
    # 子表單不加上 class
    else:
        html_code = "<ul>"
    for item in seq:
        if type(item) == type([]):
            level += 1
            html_code +="<li>"+ item[0]
            # 子表單 level 不為 0
            html_code += SequenceToUnorderedList(item[1:], 1)
            html_code += "</li>"
        else:
            html_code += "<li>%s</li>" % item
    html_code += "</ul>\n"
    return html_code

def GenerateMenu():
    return SequenceToUnorderedList(表單數列, 0) \
+'''
<script type="text/javascript" src="/static/jscript/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="/static/jscript/dropdown.js"></script>
'''
